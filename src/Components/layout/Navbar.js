import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import SignedIn from "./SignedIn";
import SignedOut from "./SignedOut";
let Navbar = props => {
  const { auth } = props;
  const link = auth.uid ? <SignedIn /> : <SignedOut />;
  return (
    <nav>
      <div className="nav-wrapper purple darken-3">
        <div className="container">
          <Link to="/" className="brand-logo">
            TakeSC
          </Link>
          {link}
        </div>
      </div>
    </nav>
  );
};
const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.firebase.auth
  };
};

export default connect(mapStateToProps)(Navbar);
