import React from "react";
import moment from "moment";

const Notification = ({ notifications }) => {
  return (
    <div className="sections">
      <div className="card z-depth-1">
        <div className="card-content">
          <span className="card-title">Notifications</span>
          <ul className="notifications">
            {notifications
              ? notifications.map(notification => {
                  return (
                    <li key={notification.id}>
                      <span className="pink-text">{notification.user} </span>
                      <span className="">{notification.content}</span>
                      <div className="gey-text">
                        {moment(notification.time.toDate()).fromNow()}
                      </div>
                    </li>
                  );
                })
              : null}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Notification;
