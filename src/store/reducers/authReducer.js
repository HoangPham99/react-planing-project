let initState = {
  authError: null
};

const authReducer = (state = initState, actions) => {
  switch (actions.type) {
    case "LOGIN_SUCCESS":
      return {
        ...state,
        authError: null
      };
    case "LOGIN_ERROR":
      return {
        ...state,
        authError: "login failed"
      };
    case "SIGNOUT_SUCCESS":
      console.log("signout success");
      return state;
    case "SIGNUP_SUCCESS":
      console.log("signup success");
      return {
        ...state,
        authError: null
      };
    case "SIGNUP_ERROR" :
      console.log('signup_error');
      console.log(actions.err);
      return {
        ...state,
        authError : actions.err.message
      }
    default:
      return state;
  }
};

export default authReducer;
