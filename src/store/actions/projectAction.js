export const createProject = project => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    // make some aync code
    let firestore = getFirestore();

    let profile = getState().firebase.profile;
    firestore
      .collection("projects")
      .add({
        ...project,
        authorFirstName: profile.firstName,
        authorLastName: profile.lastName,
        authorId: getState().firebase.auth.uid,
        createAt: new Date()
      })
      .then(() => {
        dispatch({
          type: "CREATE_PROJECT",
          project: project
        });
      })
      .catch(err => {
        dispatch({
          type: "CREATE_PROJECT_ERROR",
          err
        });
      });
  };
};
