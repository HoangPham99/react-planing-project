import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../../store/actions/authAction";

function SignedIn(props) {
  let { profile } = props;
  return (
    <ul id="nav-mobile" className="right hide-on-med-and-down">
      <li>
        <NavLink to="/create">New Project</NavLink>
      </li>
      <li>
        <a
          href="/signin"
          onClick={() => {
            props.signOut();
          }}
        >
          Log out
        </a>
      </li>
      <li>
        <NavLink className="btn btn-floating pink lighten-1" to="/">
          {profile ? profile.initials : null}
        </NavLink>
      </li>
    </ul>
  );
}

const mapDispatchtoProps = dispatch => {
  return {
    signOut: () => {
      dispatch(signOut());
    }
  };
};

const mapStatetoProps = (state) => {
  return {
    profile : state.firebase.profile
  }
}

export default connect(mapStatetoProps, mapDispatchtoProps)(SignedIn);
