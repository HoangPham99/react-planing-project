import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyC7xP-OK_0DfZ48Z1aNhVQG11JMxqGdGVk",
    authDomain: "react-plaining.firebaseapp.com",
    databaseURL: "https://react-plaining.firebaseio.com",
    projectId: "react-plaining",
    storageBucket: "react-plaining.appspot.com",
    messagingSenderId: "229019139080",
    appId: "1:229019139080:web:276198e46d3b40d7fe65bd",
    measurementId: "G-SJTX431E55"
};

firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({});

export default firebase;