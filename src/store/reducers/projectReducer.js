let initState = {};

const projectReducer = (state = initState, actions) => {
  switch (actions.type) {
    case "CREATE_PROJECT":
      console.log("create project", actions.project);
      return state;
    case "CREATE_PROJECT_ERROR":
      console.log("create project error", actions.err);
      return state;
    default:
      return state;
  }
};

export default projectReducer;
